Item {
    id: paragraph
    property string text: "Lorem ipsum blabla"
    property color color: "#66d9ef"
    height: textElement.height + 20
    anchors.left: parent.left
    anchors.right: parent.right
    Text {
        width: parent.width
        id: textElement
        x: 10
        anchors.verticalCenter: parent.verticalCenter
        text: paragraph.text
        color: paragraph.color
        font.pixelSize: 14
        font.family: "monospace"
        wrapMode: Text.Wrap
    }
}
