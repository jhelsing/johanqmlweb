import QtQuick 2.0

Item {
    id: button
    signal clicked
    property string text: "Button"
    property alias highlightColor: highlight.color
    property alias textColor: label.color
    property int padding: 10
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    width: label.width + 2*padding
    state: "inactive"
    states: [
        State {
            name: "inactive"
            PropertyChanges {
                target: highlight
                height: 0
            }
        },
        State {
            name: "active"
            PropertyChanges {
                target: highlight
                height: 5
            }
        }
    ]
    transitions: [
        Transition {
            to: "*"
            NumberAnimation { target: highlight; property: "height"; duration: 200; easing.type: Easing.OutBack; easing.overshoot: 5 }
        }
    ]
    Text {
        x: padding
        anchors.verticalCenter: parent.verticalCenter
        id: label
        color: "#ef5939"
        text: button.text
        font.pixelSize: 20
        font.family: "monospace"
    }
    Rectangle {
        id: highlight
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        color: "#70F0F0"
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            button.clicked();
        }
    }
}
