import QtQuick 2.0

Item {
    state: "inactive"
    states: [
        State {
            name: "inactive"
            PropertyChanges {
                target: highlight
                height: 0
            }
        },
        State {
            name: "active"
            PropertyChanges {
                target: highlight
                height: 5
            }
        }
    ]
    transitions: [
        Transition {
            to: "*"
            NumberAnimation { target: highlight; property: "height"; duration: 200; easing.type: Easing.OutBack; easing.overshoot: 5 }
        }
    ]
    Rectangle {
        id: highlight
        width: 200
        color: "#70F0F0"
    }
}
