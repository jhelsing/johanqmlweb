Item {
    id: header
    property string text: "Header"
    property color color: "#A6E22E"
//    anchors.left: parent.left
//    anchors.right: parent.right
    width: textElement.width
    height: textElement.height + 20
    Text {
        id: textElement
        x: 10
        anchors.verticalCenter: parent.verticalCenter
        text: header.text
        color: header.color
        font.pixelSize: 16
        font.family: "monospace"
    }
}
