import QtQuick 2.0

Item {
    property int padding: 20
    id: page
    anchors.centerIn: parent
    width: parent.width - padding*2
    height: parent.height - padding*2
    property bool active: contents.activePage === page
    visible: active
}

