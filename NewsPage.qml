Page {
    Header {
        id:date
        text: "27. feb 2014: "
        color: "#7070F0"
    }
    Header {
        anchors.left: date.right
        text: "New design"
    }
    Paragraph{
        anchors.top: date.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        width: 100
        text: "I discovered QmlWeb, and found out I wanted to make this page a little bit prettier. This is the result.\n\n\
QmlWeb was mostly pleasant to work with, although some elements from regular QML were missing. I was most annoyed about the missing childrenRect property, as well as some bugs in the alignment on the games page. Also text wrap doesn't work correctly with element width."
    }
}
