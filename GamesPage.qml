Page{
    ListModel{
        id: games
        ListElement {
            name: "Prometheus"
            description: "Bring light to world by dying repeatedly in the dark"
            year: "2014"
            browser: "http://folk.ntnu.no/johanklo/prometheus"
            download: "http://folk.ntnu.no/johanklo/prometheus/download"
            collaborators: "Sindre Næss"
            unity: true
        }
        ListElement {
            name: "Lego Scavengers"
            description: "Build your own LEGO spaceship"
            year: "2013"
            browser: "http://folk.ntnu.no/johanklo/legoscavengers"
            download: "http://folk.ntnu.no/johanklo/legoscavengers/download/"
            collaborators: "John Edvard Reiten and Renata Royzmann"
            unity: true
        }
        ListElement {
            name: "Space Battle"
            description: "Two player space combat"
            year: "2013"
            browser: "spacebattle"
            collaborators: "Alan Fernandez"
            unity: false
        }
        ListElement {
            name: "Space Invader"
            description: "You play the last bad guy invading a planet"
            year: "2012"
            browser: "spaceinvader"
            collaborators: "Alan Fernandez"
            unity: false
        }
        ListElement {
            name: "Dual Monopod"
            description: "Networked card game for android inspired by Final Fantasy"
            year: "2012"
            download: "dualmonopod"
            collaborators: "John Edvard Reiten and Aleksander Sjåfjell"
            unity: false
        }
    }

    Column{
        anchors.fill:parent
        Repeater {
            model: games
            Item {
                id: expandable
                anchors.left: parent.left
                anchors.right: parent.right
                height: header.height+gameDrawer.height
                state: "collapsed"
                states: [
                    State {
                        name: "collapsed"
                        PropertyChanges {
                            target: gameDrawer
                            height: 0
                            visible: false
                        }
                    },
                    State {
                        name: "expanded"
                        PropertyChanges {
                            target: gameDrawer
                            height: gameDrawer.preferredHeight
                            visible: true
                        }
                    }
                ]
                transitions: [
                    Transition {
                        to: "expanded"
                        NumberAnimation {
                            target: gameDrawer
                            property: "height"
                            duration: 400
                            easing.type: Easing.OutBack
                            //easing.overshoot: 5
                        }
                    }
                ]
                Header {
                    id: header
                    height: 20
                    width: 10
                    text: "[" + (expandable.state === "collapsed" ? "+" : "-") + "] " + name + " (" + year + ")"
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            expandable.state = expandable.state === "collapsed" ? "expanded" : "collapsed";
                        }
                    }
                }
                Item {
                    id: gameDrawer
                    visible: false 
                    anchors.top: header.bottom
                    //height: childrenRect.height //childrenrect is not yet implemented
                    property int preferredHeight: (download!==undefined ? downloadHdr.height : 0) +
                    (browser!==undefined ? browserHdr.height : 0) +
                    descriptionP.height + creditsP.height
                    anchors.right: parent.right
                    width: parent.width - 40
                    Column {
                        anchors.fill:parent
                        Paragraph{
                            id: descriptionP
                            text: description
                        }
                        Paragraph{
                            id: creditsP
                            text: "Made in collaboration with "+ collaborators
                        }
                        Header{
                            color: "#FF0000"
                            id: browserHdr
                            visible: browser !== undefined
                            text: "Play in browser "+ 
                            (unity ? "(requires unity)" : "")
                            +"<a href=\""+browser+"\">"+browser+"</a>"
                        }
                        Header{
                            color: "#FF0000"
                            id: downloadHdr
                            visible: download !== undefined
                            text: "Download <a href=\""+download+"\">"+download+"</a>"
                        }
                    }
                }
            }
        }
    }
}
