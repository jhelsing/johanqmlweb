import QtQuick 2.0

Rectangle {
    anchors.fill: parent
    color: "#232526"
    Item {
        id: header
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 40
        Row {
            anchors.fill: parent
            Item{width:10; height:1}
            Text {
                id: headerText
                anchors.verticalCenter: parent.verticalCenter
                text: "Johan Klokkhammer Helsing"
                color: "#AE81FF"
                font.pixelSize: 20
                font.family: "monospace"
            }
            Item{width:10; height:1}
            ListModel {
                id: pages
                ListElement {
                    name: "news"
                    page: news
                }
                ListElement {
                    name: "games"
                    page: games
                }
//                ListElement {
//                    name: "contact"
//                    page: contact
//                }
            }
            Repeater {
                id: topMenu
                property string activeButton: "news"
                model: pages
                TopBarButton {
                    text: name
                    state: topMenu.activeButton === name ? "active" : "inactive"
                    onClicked: {
                        console.log(name);
                        topMenu.activeButton = name;
                        contents.activePage = page;
                    }
                }
            }
        }
        //games
        //contact
        //cv
    }
    Rectangle {
        id: contents
        property var activePage: news
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        radius: 10
        color: "#1B1D1E"
        NewsPage {
            id: news
        }
        GamesPage {
            id: games
        }
        ContactPage {
            id: contact
        }
    }
}
